Rails.application.routes.draw do
  resources :microposts

  resources :users

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".